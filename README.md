# Curriculum Vitae for Njagi Mwaniki

A CV written in LaTeX using the [moderncv](http://www.ctan.org/pkg/moderncv) class.

## Sample
Here's what it looks like with real data, using the "classic" style and blue color scheme:

![Image](https://i.imgur.com/jm0Cdv9.png)

## Why

I decided to start keeping my CV in LaTeX because:

* Easy to maintain, update and so forth (or so I tell myself).
* It's goddamn tex.
* Using a git repo means I can get my CV and how to generate it asap.
* It's cool.
* Makes me happy.

## Usage

To "build" a PDF you simply type:

	make

To clean up all generated and intermediate content, type:

	make clean

## Pre-requistes for building

Depending on your GNU/Linux distribution the package names may be different.  Basically, you need the `texlive` package, as well as whichever "extras" package contains the
moderncv stuff.

__On Arch Linux:__

	sudo pacman -Sy texlive-core texlive-latexextra

__On Fedora:__

	sudo yum install texlive texlive-moderncv


__On Ubuntu:__

(If you play around enough you'll realise that you can fetch more files than you need and by that I mean a ton so I'll give just what you need for moderncv plus the modernstuff.)

Note currently has texlive 2012 I expect they will add texlive 2013 to the ppa soon. Last I checked the repos had texlive 2009 so...

     sudo add-apt-repository ppa:texlive-backports
     sudo apt-get install texlive
     sudo apt-get --no-install-recommends install texlive-latex-extra


__Others:__ send a pull request with instructions for your distro.
